const path = require("path")
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const StyleLintPlugin = require("stylelint-webpack-plugin")
const CopyWebpackPlugin = require('copy-webpack-plugin')
const LessCleanCSS = require("less-plugin-clean-css")
const LessAutoPrefix = require("less-plugin-autoprefix")


module.exports = {
    context: path.join(__dirname, 'src'),
    entry : {
        app : path.resolve(__dirname,"./src/js/app.js")
    },
    output : {
        path : path.resolve(__dirname,"dist"),
        filename : "./js/app.js"
    },
    module : {
        rules : [
            {
                test : /\.less$/,
                use : ExtractTextPlugin.extract({
                    use : [
                        {
                            loader : "css-loader"
                        },
                        {
                            loader : "less-loader",
                            options : {
                                plugins : [
                                    new LessAutoPrefix({browsers: ["cover 95%"]}),
                                    new LessCleanCSS({advanced : true})
                                ],
                            }
        
                        }
                    ]
                })
            },
            {
                test: /\.woff2$/,
                use: {
                  loader: "url-loader",
                  options: {
                    limit: 50000,
                  },
                },
              },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            "es2015",
                            "es2016"
                        ]
                    }
                }
            }
        ]
    },
    devServer: {
        stats: "errors-only",
        host: process.env.HOST,
        port: process.env.PORT,
        open: true,
        overlay: true,
        compress: true,
        watchContentBase: true
    },
    plugins : [
        new ExtractTextPlugin("./styles/styles.css"),
        new StyleLintPlugin(),
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname,"index.html"),
                to: './'
            }
        ])
    ]
}
