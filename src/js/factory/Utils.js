import _ from "underscore";

class Utils{

    static getById(id, elements){
        let element;
        if(_.isUndefined(id)){
            element = _.first(elements);
        }
        else{
            element = _.find(elements,function(_element){
                return _element.id == id;
            });
        }
        return element;
    }
    
}

export default Utils;