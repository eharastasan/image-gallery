import _ from "underscore";
import AppService from './AppService';

class TranslateService{

    constructor(){
        this.availableLang = {
            "EN" : [],
            "ES" : []
        };
        this.defaultLang = "EN";
        this.getTranslates();
    }

    getTranslates(){
        let availableLangs = Object.keys(this.availableLang);
        for (let index = 0; index < availableLangs.length; index++) {
            let currentLang = availableLangs[index];
            this.availableLang[currentLang] = require("./../../resources/translates/" + currentLang + ".json");

            if(this.defaultLang == currentLang){
                TranslateService.currentTranslations = this.availableLang[currentLang];
            }
        }
    }

    translateAppConfig(){
        var self = this;
        _.forEach(AppService.appConfig,function(config){
            _.forEach(config,function(param){
                param.translate = TranslateService.getTranslate(param.translateKey);
            });
        })
    }

    static getTranslate(translateKey){
        let translate = translateKey;
        try{
            translate = TranslateService._getRecursively(translateKey,TranslateService.currentTranslations);
        }
        catch(e){
            console.warn(e + " -> " + translateKey);
        }
        return translate;
    }

    static _getRecursively(translateKey, list){
        let currentKey = translateKey.split(".")[0];
        let currentList = list[currentKey];

        if(typeof currentList === "string"){
            return currentList;
        }
        else if(!_.isUndefined(currentList)){
            let innerKey = translateKey.replace(currentKey + ".", "");
            return this._getRecursively(innerKey,currentList);
        }
        else throw "KeyNotFound";
    }
}

export default TranslateService;