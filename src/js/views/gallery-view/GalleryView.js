import $ from 'jquery';
import _ from "underscore";
import ImageThumbnailView from './ImageThumbnailView';
import HttpService from './../../services/HttpService';

class GalleryView{
    
    constructor(galleryMenuView){
        this.galleryElement = $("#gallery-view");
        this.httpService = new HttpService();
        this.galleryMenuView = galleryMenuView;
        this.bindGalleryScroll();
        this.currentPageNumber = 1;
    }

    setImagesThumbnail(images) {
        let self = this;
        _.forEach(images, function(image){
            let imageThumbnailView = new ImageThumbnailView(image,self);
            self.galleryElement.append(imageThumbnailView.createElement());
        });
    }

    clearImages(){
        this.galleryElement.html("");
    }

    bindGalleryScroll(){
        let self = this;

        $(self.galleryElement).scroll(function() {
            let currentScrollPosition = self.galleryElement.scrollTop() + self.galleryElement.innerHeight();
            let scrollHeight = self.galleryElement.prop("scrollHeight");
            if(currentScrollPosition == scrollHeight){
                setTimeout(function(){
                    self.currentPageNumber++;
                    self.addMoreImagesToGallery(self.currentPageNumber,false).then(() =>{
                        // TODO add loader
                    });
                },250);
            }
        });
    }

    addMoreImagesToGallery(currentPageNumber,resetImagesList){
        let self = this;
        return new Promise((resolve , reject) => {
            self.currentPageNumber = currentPageNumber;
            self.httpService.buildApiUrl(self.galleryMenuView.galleryMenu);
            self.httpService.getGalleryList(currentPageNumber,resetImagesList).then(function(images){
                self.setImagesThumbnail(images);
                resolve(images);
            },function(err){
                reject(err);
            });
        });
    }
}

export default GalleryView;