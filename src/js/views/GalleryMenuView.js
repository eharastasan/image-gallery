import $ from 'jquery';
import _ from "underscore";
import GalleryMenu from "./../models/GalleryMenu";
import ElementsFactory from "./../factory/ElementsFactory";

class GalleryMenuView{

    constructor(){
        this.galleryMenuElement = $("#gallery-menu-view");
        this.galleryMenu = new GalleryMenu();
        this.generatedElements = [];
        this.isHiddenMenu = (window.innerWidth < 900);
    }

    generateElements(){        
        this.queryInput = ElementsFactory.createInput(this.galleryMenu.query, "menu-option");
        
        this.categorySelect = ElementsFactory.createSelect(this.galleryMenu.category, "menu-option");
        this.typeSelect = ElementsFactory.createSelect(this.galleryMenu.image_type, "menu-option");
        this.imageSizeSelect = ElementsFactory.createSelect(this.galleryMenu.size, "menu-option");
        this.colorsSelect = ElementsFactory.createSelect(this.galleryMenu.color, "menu-option");
        this.searchLanguatgeSelect = ElementsFactory.createSelect(this.galleryMenu.lang, "menu-option");
        
        this.orientationRadio = ElementsFactory.createRadioButtonsGroup(this.galleryMenu.orientation,"menu-option");
        this.orderRadio = ElementsFactory.createRadioButtonsGroup(this.galleryMenu.order,"menu-option");
        this.editorsChoice = ElementsFactory.createRadioButtonsGroup(this.galleryMenu.editors_choice, "menu-option");
        this.menuFabIcon = ElementsFactory.createFabIcon();
    }

    appendElements(){
        var self = this;
        _.forEach(self,function(_prop){
            if((_prop instanceof $) && _prop.hasClass("menu-option-wrapper")){
                self.generatedElements.push(_prop);
                self.galleryMenuElement.append(_prop);
            }
        })
        $(self.menuFabIcon).appendTo("body");
    }

    addListeners(callback){
        var self = this;

        _.forEach(self.generatedElements,function(element){
            element.on("change",function(){
                let _value = element.find("input[type=radio]:checked").val() ? element.find("input[type=radio]:checked").val() : element.find(".data-source").val();
                let _propertyId = element.attr("id");
                
                let currentElementModel = _.find(self.galleryMenu,function(model){
                    return model.elementKey == _propertyId;
                });
                currentElementModel.setPropertyById(_value);

                if(callback && typeof callback == "function") callback();
            });
        });

        self.setMenuStatus();
        self.menuFabIcon.on("click",function(){
            self.setMenuStatus();
        });

        if(callback && typeof callback == "function") callback();
    }

    setMenuStatus(){
        var self = this;
        self.isHiddenMenu = !self.isHiddenMenu;
        if(self.isHiddenMenu){
            $(self.menuFabIcon).addClass("is-hidden-menu");
            self.galleryMenuElement.css({
                "animation": "show-menu .5s",
                "position": "initial"
            });
        }
        else{
            $(self.menuFabIcon).removeClass("is-hidden-menu");
            self.galleryMenuElement.css({
                "animation": "hide-menu .5s",
                "position": "absolute",
                "left": "-300px"
            });
        }
    }
}
export default GalleryMenuView;