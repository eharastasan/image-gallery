This project is a basic image gallery front-end using [Pixabay API](https://pixabay.com/api/docs/). The project's finality is to create a website where you can browse photos using a basic inputs like : search term, images type, images sizes, images color and order this search... This project is created with academic propouse and under GPL-v3 license. If want to help you're welcome.

**[Working demo on Gitlab's Artifacts](https://emidev98.gitlab.io/image-gallery)**

# About technology used

The main language of this porject is JavaScript **Class** Oriented. To simplify the usage I decided to implement some libraries. All implemented libraries are to add more browser compatibility and Webpack to build the project using babel's plugins. Talking about compatibility, this project will not be compatible with Internet Explorer.

- [jQuery](https://jquery.com/)
- [UnderscoreJs](https://underscorejs.org)
- [LessCss](http://lesscss.org)
- [Webpack](https://webpack.js.org)

# Environment setup

First of all we need to install [NodeJs](https://nodejs.org/en/)^10.0.0. Before that need to clone the project with [GIT](https://git-scm.com/) or download from GitLab by the way. Once NodeJs is installed and project is downloaded open the terminal, navigate to project plder and execute *npm install*, to install all dependencies. When the dependencies download process finish you can execute the development environment with *npm start* and open the browser on localhost:8080. If you want to build the project, should execute *npm run build:prod*, and this process will create a folder in project root folder named *dist* with all files compiled ready to upload in a webserver.   

# Guides to develop

As I mentioned this project its with academical propuses, and this section will be like a "bibliography" with all usefoul information I found to make this project.

- [JavaScript Class Oriented](https://www.digitalocean.com/community/tutorials/understanding-classes-in-javascript);
- [JavaScript programming patterns](http://crushlovely.com/journal/7-patterns-to-refactor-javascript-applications-service-objects/)